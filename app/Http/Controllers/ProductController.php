<?php

namespace App\Http\Controllers;

use App\Models\LoginData;
use App\Models\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class ProductController extends Controller
{
    //View Section
    public function index()
    {
        $Data = DB::table('products')
            ->join('product_type', 'product_type.product_type_id', '=', 'products.product_type_id')
            ->join('merchants', 'merchants.me_id', '=', 'products.merchants_id')
            ->join('login_data', 'login_data.ld_id', '=', 'merchants.me_ld_id')
            ->where('ld_id', auth()->user()->ld_id)
            ->get();

        return view('products.index', [
            'data' => $Data
        ]);
    }

    //Create Section
    public function create()
    {
        $DataType = DB::table('product_type')->get();
        return view('products.create', [
            'data' => $DataType
        ]);
    }

    public function insert(Request $request)
    {
        $me = Merchant::where('me_ld_id', auth()->user()->ld_id)->first();
        DB::table('products')->insert([
            'merchants_id' => $me->me_id,
            'product_code' => $request->ProductCode,
            'product_type_id' => $request->ProductType,
            'product_name' => $request->ProductName,
            'stock' => $request->stock,
            'price' => $request->price,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('product');
    }

    //Edit Section
    public function edit($id)
    {

        $Data = DB::table('products')
            ->join('product_type', 'product_type.product_type_id', '=', 'products.product_type_id')
            ->join('merchants', 'merchants.me_id', '=', 'products.merchants_id')
            ->where([
                'product_id' => $id,
                'merchants_id' => 1
            ])
            ->first();
        $DataType = DB::table('product_type')->get();
        return view('products.edit', [

            'data' => $DataType,
            'dt' => $Data
        ]);
    }

    public function update(Request $request)
    {
        $me = Merchant::where('me_ld_id', auth()->user()->ld_id)->first();
        DB::table('products')->where('product_id', $request->ProductId)->update([
            'merchants_id' => $me->me_id,
            'product_code' => $request->ProductCode,
            'product_type_id' => $request->ProductType,
            'product_name' => $request->ProductName,
            'stock' => $request->stock,
            'price' => $request->price,
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('product');
    }

    //Delete Section

    public function delete($id)
    {
        DB::table('products')->where('product_id', $id)->delete();

        return redirect()->route('product');
    }
}
