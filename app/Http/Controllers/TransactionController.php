<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Models\products;
use App\Models\transaction;
use App\Models\LoginData;
use Illuminate\Support\Facades\Response;
use AgliPanci\LaravelCase\Query\CaseBuilder;
use App\Models\Merchant;

class TransactionController extends Controller
{
    public function add_transaction(Request  $request)
    {
        $me = Merchant::where('me_ld_id', auth()->user()->ld_id)->first();
        $product = products::join('merchants', 'merchants.me_id', '=', 'products.merchants_id')
            ->where(
                'me_ld_id',
                $request->session()->get('loginId')
            )
            ->where('merchants_id', $me->me_id)
            ->get();

        return view('transaction.addTransaction', [
            'data' => $product
        ]);
    }
    public function GetDataProduct($id)
    {
        $me = Merchant::where('me_ld_id', auth()->user()->ld_id)->first();
        $product = products::join('merchants', 'merchants.me_id', '=', 'products.merchants_id')
            ->where('product_id', $id)
            ->where('merchants_id', $me->me_id)
            ->first();
            return Response::json($product);

    }

    public function index(Request $request)
    {
        $me = Merchant::where('me_ld_id', auth()->user()->ld_id)->first();
        $transaction = transaction::join('merchants', 'merchants.me_id', '=', 'transactions.me_id')
        ->selectRaw("transactions.tr_id,
        transactions.tr_date,
        transactions.tr_date,
        transactions.tr_type,
        transactions.tr_total,
        transactions.tr_date,
        transactions.notes,
        transactions.me_id,
        merchants.me_name,
        merchants.me_seller_name,
        merchants.me_ld_id,
        (CASE transactions.tr_type WHEN 1 THEN 'Expenses' when 2 then 'Income' END) AS status")
        ->where('transactions.me_id', $me->me_id)
        ->get();


        // dd($transaction);
        return view('transaction.index', [
            'data' => $transaction,
            'meId' => $me->me_id
        ]);
    }

    public function Create($id)
    {

        return view('transaction.create',[
            'id' => $id
        ]);
    }

    public function insert(Request $request)
    {
        $transaction = new transaction();
        $transaction->me_id = $request->me_id;
        $transaction->tr_type = $request->type;
        $transaction->tr_total = 0;
        $transaction->tr_date = $request->tr_date;
        $transaction->notes = $request->note;
        $transaction->save();

        $tr_id = transaction::select('tr_id')->latest()->first();
        return redirect()->route('add_transaction',['me_id' => $request->me_id, 'tr_id' => $tr_id]);
    }

    public function edit($id)
    {
        $transaction = transaction::join('merchants', 'merchants.me_id', '=', 'transactions.me_id')
        ->selectRaw("transactions.tr_id,
        transactions.tr_date,
        transactions.tr_date,
        transactions.tr_type,
        transactions.tr_total,
        transactions.tr_date,
        transactions.notes,
        transactions.me_id,
        merchants.me_name,
        merchants.me_seller_name,
        merchants.me_ld_id,
        (CASE transactions.tr_type WHEN 1 THEN 'Expenses' when 2 then 'Income' END) AS status")
        ->where('transactions.tr_id', $id)
        ->first();

        return view('transaction.edit', [
            'dt' => $transaction
        ]);
    }

    public function update(Request $request)
    {
        $transaction = transaction::find($request->id);
        $transaction->tr_type = $request->type;
        $transaction->tr_date = $request->tr_date;
        $transaction->notes = $request->note;
        $transaction->save();

        return redirect()->route('transactions');
    }

    public function delete($id)
    {

        $transaction = transaction::where('tr_id', $id)->first();
        $transaction->delete();

        return redirect()->route('transactions');
    }
}
