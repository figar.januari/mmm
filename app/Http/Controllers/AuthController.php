<?php

namespace App\Http\Controllers;

use App\Models\LoginData;
use App\Models\Merchant;
use App\Models\transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
  public function dashboard()
  {
    $me = Merchant::where('me_ld_id', auth()->user()->ld_id)->first();
    $pengeluaran = transaction::join('merchants', 'merchants.me_id', '=', 'transactions.me_id')
        ->where('transactions.me_id', $me->me_id)
        ->where('transactions.tr_type', 1)
        ->sum('tr_total');
    $pengeluaran = $pengeluaran ?? 0;
    $pemasukkan = transaction::join('merchants', 'merchants.me_id', '=', 'transactions.me_id')
        ->where('transactions.me_id', $me->me_id)
        ->where('transactions.tr_type', 2)
        ->sum('tr_total');
    $pemasukkan = $pemasukkan ?? 0;

    return view('profile.keren', [
      'pengeluaran' => "Rp " . number_format($pengeluaran,2,',','.'),
      'laba_bersih' =>  "Rp " . number_format($pemasukkan-$pengeluaran,2,',','.'),
      'laba_kotor' =>  "Rp " . number_format($pemasukkan,2,',','.')
    ]);
  }

  public function signup()
  {
    return view('auth.signup');
  }

  public function doRegister(Request $request)
  {
    $request->validate([
      'username' => ['required', 'unique:App\Models\LoginData,ld_username', 'max:25'],
      'password' => ['required'],
    ]);

    $loginData = new LoginData();
    $loginData->ld_username = $request->username;
    $loginData->ld_password = sha1($request->password);
    $loginData->save();
    $ldId = $loginData->ld_id;

    $merchantData = new Merchant();
    $merchantData->me_ld_id = $ldId;
    $merchantData->me_name = $request->storeName;
    $merchantData->me_seller_name = $request->fullname;
    $merchantData->me_address = $request->address;
    $merchantData->me_phone = $request->phoneNumber;
    $merchantData->me_email = $request->email;
    $merchantData->me_type = $request->type;
    $merchantData->me_register_date = Carbon::now();
    $merchantData->save();

    Auth::login($loginData);
    return redirect()->route('dashboard');
  }

  public function do_login(Request $request)
  {
    Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

      $request->validate([
          'username' => 'required',
          'password' => ['required'],
      ]);

      $ld = LoginData::where(['ld_username' => $request->username, 'ld_password' => sha1($request->password)])->first();
      if ($ld)  {
        Auth::login($ld);
        return redirect()->route('dashboard');
      }

      return back()->withErrors([
          'password' => 'Wrong username or password',
      ]);
  }

  public function do_logout(Request $request)
  {
    Auth::logout();
    $request->session()->invalidate();
    $request->session()->regenerateToken();
    return redirect()->route('login');
  }
}
