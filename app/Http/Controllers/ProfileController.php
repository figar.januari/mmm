<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        Session::put('loginId', 1);

        $profileData = Merchant::where('me_ld_id', $request->session()->get('loginId'))->first();
        $profileData2 = DB::select('select * from merchants where me_ld_id = ? limit 1', [$request->session()->get('loginId')]);

        return view('profile.profile_index', [
            'data' => $profileData2[0]
        ]);
    }
    
    public function update(Request $request)
    {
        $merchant = Merchant::where('me_ld_id', $request->id)->first();
        $merchant->me_name = $request->merchantName;
        $merchant->save();

        return redirect()->route('profile');
    }

    public function insert(Request $request)
    {
        $merchant = new Merchant();
        $merchant->me_name = $request->merchantName;
        $merchant->me_ld_id = $request->session()->get('loginId');
        $merchant->me_seller_name = $request->sellerName;
        $merchant->me_address = "test";
        $merchant->me_phone = "test";
        $merchant->me_email = "test";
        $merchant->me_type = "test";
        $merchant->me_register_date = "2022-09-09 10:10:10";
        $merchant->save();

        return redirect()->route('profile');
    }

    public function delete(Request $request)
    {
        $merchant = Merchant::where('me_id', $request->id)->first();
        $merchant->delete();

        return redirect()->route('profile');
    }
}
