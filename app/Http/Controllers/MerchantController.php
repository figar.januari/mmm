<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Models\Merchant;
use App\Models\LoginData;

class MerchantController extends Controller
{
    public function detail(Request $request)
    {
        $MerchantDetail = Merchant::join('login_data', 'login_data.ld_id', '=', 'merchants.me_ld_id')
            ->where(
                'me_ld_id',
                auth()->user()->ld_id
            )->first();

        return view('merchant.detail', [
            'dt' => $MerchantDetail
        ]);
    }

    public function index(Request $request)
    {
        $Merchant = Merchant::all();

        return view('merchant.index', [
            'data' => $Merchant
        ]);
    }

    public function Create()
    {

        return view('merchant.create');
    }

    public function insert(Request $request)
    {
        $merchant = new Merchant();
        $merchant->me_name = $request->MerchantName;
        $merchant->me_ld_id = auth()->user()->ld_id;
        $merchant->me_seller_name = $request->SellerName;
        $merchant->me_address = $request->address;
        $merchant->me_phone = $request->phone;
        $merchant->me_email = $request->email;
        $merchant->me_type = $request->merchantType;
        $merchant->me_register_date = Carbon::now();
        $merchant->save();

        return redirect()->route('merchant');
    }

    //Edit Section
    public function edit($id, $custom)
    {
        $merchant = Merchant::join('login_data', 'login_data.ld_id', '=', 'merchants.me_ld_id')
            ->where('me_id', $id)->first();

        return view('merchant.edit', [
            'dt' => $merchant,
            'custom' => $custom
        ]);
    }

    public function update(Request $request)
    {
        $merchant = Merchant::find($request->id);
        $merchant->me_name = $request->MerchantName;
        $merchant->me_ld_id = auth()->user()->ld_id;
        $merchant->me_seller_name = $request->SellerName;
        $merchant->me_address = $request->address;
        $merchant->me_phone = $request->phone;
        $merchant->me_email = $request->email;
        $merchant->me_type = $request->merchantType;
        $merchant->me_register_date = Carbon::now();
        $merchant->save();

        return redirect()->route('merchant');
    }
    public function delete($id, $userId)
    {
        $user = LoginData::where('ld_id', $userId)->first();
        $user->delete();
        $merchant = Merchant::where('me_id', $id)->first();
        $merchant->delete();

        return redirect()->route('merchant');
    }
}
