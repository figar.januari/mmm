<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class ProductTypeController extends Controller
{
        //View Section
        public function index()
        {
            //Session::put('loginId', 1);
    
            $Data = DB::table('product_type')->get();
    
            return view('productType.index', [
                'data' => $Data
            ]);
        }

        //Create Section
        public function create()
        {
            
            return view('productType.create');
        }

        public function insert(Request $request)
        {
            $time = now()->timestamp; 
            DB::table('product_type')->insert([
                'product_type_name' => $request->ProductTypeName
            ]);

            return redirect()->route('product_type');
        }

        //Edit Section
        public function edit($id)
        {
            
            $Data = DB::table('product_type')
            ->where('product_type_id', $id)
            ->first();

            return view('productType.edit',[
                'dt' => $Data
            ]);
        }

        public function update(Request $request)
        {
            
            DB::table('product_type')->where('product_type_id',$request->ProductTypeId)->update([
                'product_type_name' => $request->ProductTypeName
            ]);

            return redirect()->route('product_type');
        }

        //Delete Section

        public function delete($id)
        {
            DB::table('product_type')->where('product_type_id',$id)->delete();
            
            return redirect()->route('product_type');
        }

}
