<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Models\products;
use App\Models\transaction;
use App\Models\transaction_details;
use App\Models\LoginData;
use App\Models\Merchant;
use Illuminate\Support\Facades\Response;


class TransactionDetail extends Controller
{
    public function index($id,$type,$me_id)
    {
        $product = transaction_details::join('products', 'products.product_id', '=', 'transaction_details.product_code')
        ->where('transaction_details.tr_id', $id)
        ->get();
        return view('transactionDetail.index', [
            'data' => $product,
            'type' => $type,
            'me_id' => $me_id
        ]);
    }


    public function add_transaction($me_id,$tr_id)
    {
        //Session::put('loginId', 1);

        $product = products::join('merchants', 'merchants.me_id', '=', 'products.merchants_id')
            ->where('merchants_id', $me_id)
            ->get();

        $sum_tr = transaction_details::where('tr_id', $tr_id)->sum('total_price');
            //return response()->json($product);
        // dd($product);
        return view('transactionDetail.addTransaction', [
            'data' => $product,
            'tr_id' => $tr_id,
            'sum_tr' => $sum_tr
        ]);
    }
    public function GetDataProduct($id)
    {
        $me = Merchant::where('me_ld_id', auth()->user()->ld_id)->first();
        $product = products::join('merchants', 'merchants.me_id', '=', 'products.merchants_id')
            ->where('product_id', $id)
            ->where('merchants_id', $me->me_id)
            ->first();
            return Response::json($product);

    }

    public function insert(Request $request)
    {
        $totalPrice = $request->price ;
        $Sumprice = $request->sum_tr;
        //dd($request);
        $transaction_sum = transaction::find($request->tr_id);
        $transaction_sum->tr_total = $totalPrice + $Sumprice;
        $transaction_sum->save();

        $transaction = new transaction_details();
        $transaction->tr_id = $request->tr_id;
        $transaction->me_id = $request->me_id;
        $transaction->product_code = $request->product;
        $transaction->total_stock = $request->stock;
        $transaction->total_price = $request->price;
        $transaction->save();

        return redirect()->route('transactions');
    }

    public function delete($id,$tr_id)
    {


            $transaction = transaction_details::where('tr_dt_id', $id)->first();
            $transaction->delete();
            $sum_tr = transaction_details::where('tr_id', $tr_id)->sum('total_price');
            //dd($sum_tr);
            if($sum_tr != "0"){
                $transaction = transaction::find($tr_id);
                $transaction->tr_total = $sum_tr;
                $transaction->save();
            }
            if($sum_tr == 0){
                $transaction = transaction::where('tr_id', $tr_id)->first();
                $transaction->delete();
            }
        return redirect()->route('transactions');
    }
}
