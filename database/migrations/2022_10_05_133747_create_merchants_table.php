<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->id('me_id');
            $table->integer('me_ld_id');
            $table->string('me_name')->nullable();
            $table->string('me_seller_name')->nullable();
            $table->string('me_address')->nullable();
            $table->string('me_phone')->nullable();
            $table->string('me_email')->nullable();
            $table->string('me_type')->nullable();
            $table->dateTime('me_register_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
};
