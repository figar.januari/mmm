<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id('product_id');
            $table->integer('merchants_id');
            $table->string('product_code')->nullable();
            $table->text('product_name')->nullable();
            $table->integer('product_type_id')->nullable();
            $table->decimal('price')->nullable();
            $table->dateTime('entrydate')->nullable();
            $table->string('userNameEntry')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
