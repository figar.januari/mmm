@extends('welcome')

@section('content')
<div class="container-fluid py-4">
  <form action="{{ route('product_insert') }}" method="post">
    @csrf
    <input type="hidden" value="" name="id">
    <div class="row">
      <div class="col-md-12">
        <div class="card">

          <div class="card-body">
            <h4>Transaction</h4>
            <div class="row">

              <div class="col-md-8">
                <div class="form-group">
                  <label for="example-text-input" class="form-control-label">Product Type</label>
                  <select class="js-example-basic-single form-control" name="product[]" id="product">
                    <option value=""> Select Option</option>
                    @foreach ($data as $item)
                    <option value="{{ $item->product_id }}">{{ $item->product_name }}
                    </option>
                    @endforeach
                  </select>
                </div>
                <div>
                  <label for="example-text-input" class="form-control-label">Stock</label>
                  <input class="form-control" type="Number" id="stock" name="stock" value="">
                  <input class="form-control" type="Number" id="me_id" name="me_id" value="">
                </div>
                <div class="mb-3">
                  <label for="example-text-input" class="form-control-label">Price</label>
                  <input class="form-control" type="Number" id="price" name="price" value="">
                </div>

                <div class="mb-3">
                  <label for="example-text-input" class="form-control-label">Transaction Type</label>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="type" id="customRadio1" checked="">
                    <label class="custom-control-label" for="customRadio1">Expenses</label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="type" id="customRadio2">
                    <label class="custom-control-label" for="customRadio2">Income</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Note</label>
                  <textarea class="form-control" id="note" name="note" rows="3"></textarea>
                </div>

              </div>
            </div>


            <input type="submit" value="Save" class="btn btn-primary">
            <a href="{{ route('product') }}" class="btn btn-primary" data-toggle="tooltip">
              Back
            </a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script>
  $('#product').on('change', function () {
    //alert(this.value);
    const id = this.value;
    //alert(window.location.origin);
    let HostName = window.location.origin;
    var url = HostName + "/transaction/GetDataProduct/" + id;
    //alert(url);
    $.getJSON(url, function (data) {
      //alert(data["stock"]);
      $('#stock').val(data["stock"]);
      $('#price').val(data["price"]);
      $('#me_id').val(data["me_id"]);
    });
  });

</script>

@endsection
