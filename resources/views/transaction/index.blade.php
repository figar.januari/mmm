@extends('welcome')

@section('content')

<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          <H3>Transactions</H3>

        </div>

        <div class="card-body p-5">
          <a href="{{ route('transactions_form_create', ['id' => $meId]) }}" class="btn btn-primary" data-toggle="tooltip"
            data-original-title="Edit user">
            Create Transaction
          </a>
          <!-- <input type="text" id="min" name="min">
          <input type="text" id="max" name="max"> -->
          <div class="table-responsive p-0">
            <table class="table align-items-center mt-5" style="font-size:12px;" id="myTable2">
              <thead>
                <tr>
                  <th>Merchant Name</th>
                  <th>Seller Name</th>
                  <th>Transaction Type</th>
                  <th>Transaction Total</th>
                  <th>Transaction Date</th>
                  <th>Notes</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $k => $item)
                <tr align="center">

                  <td>{{$item->me_name}}</td>
                  <td>{{$item->me_seller_name}}</td>
                  <td>{{$item->status}}</td>
                  <td>
                    {{$item->tr_total}}
                  </td>
                  <td>{{ \Carbon\Carbon::parse($item->tr_date)->format('Y-m-d') }}</td>
                  <td>
                    {{$item-> notes}}
                  </td>
                  <td class="align-middle">
                    <a href="{{ route('transactions_detail', ['id' => $item->tr_id,'type' => $item->status,'me_id'=> $item->me_id]) }}"
                      class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                      data-original-title="Edit user">
                      Details
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('transactions_form_edit', ['id' => $item->tr_id]) }}"
                      class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                      data-original-title="Edit user">
                      Edit
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('transactions_delete', ['id' => $item->tr_id]) }}"
                      class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                      data-original-title="Edit user">
                      Delete
                    </a>
                  </td>
                </tr>
                @endforeach


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
