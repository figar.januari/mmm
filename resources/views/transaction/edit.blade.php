@extends('welcome')

@section('content')
<div class="container-fluid py-4">
  <form action="{{route('transactions_update')}}" method="post">
    @csrf
    <input type="hidden" value="" name="id">
    <div class="row">
      <div class="col-md-12">
        <div class="card">

          <div class="card-body">
            <h4>Transaction</h4>
            <div class="row">

              <div class="col-md-8">

                <div class="form-group">
                  <label for="example-date-input" class="form-control-label">Transaction Date</label>
                  <input class="form-control" type="text" value="{{ \Carbon\Carbon::parse($dt->tr_date)->format('Y-m-d') }}" id="tr_date" name="tr_date">
                </div>

                <div class="mb-3">
                  <label for="example-text-input" class="form-control-label">Transaction Type</label>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" value="1" name="type" id="customRadio1"  
                    @if ($dt->tr_type == 1) checked="" @endif>
                    <label class="custom-control-label" for="customRadio1">Expenses</label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="type" value="2" id="customRadio2"
                    @if ($dt->tr_type == 2) checked="" @endif>
                    <label class="custom-control-label" for="customRadio2">Income</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Note</label>
                  <textarea class="form-control" id="note" name="note"  rows="3">{{$dt->notes}}</textarea>
                  <input class="form-control" type="hidden" value="{{$dt->tr_id}}" id="id" name="id">
                </div>

              </div>
            </div>


            <input type="submit" value="Save" class="btn btn-primary">
            <a href="{{ route('transactions') }}" class="btn btn-primary" data-toggle="tooltip">
              Back
            </a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>

<script>
    $(document).ready(function() {
        new DateTime($('#tr_date'), {
                format: 'YYYY-MM-DD'
            });
    });
</script>
@endsection
