@extends('welcome')

@section('content')

<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          <H3>Merchants</H3>

        </div>

        <div class="card-body p-5">
          <!-- <a href="{{ route('merchant_form_create') }}" class="btn btn-primary" data-toggle="tooltip"
            data-original-title="Edit user">
            Create Merchant
          </a> -->
          <!-- <input type="text" id="min" name="min">
          <input type="text" id="max" name="max"> -->
          <div class="table-responsive p-0">
            <table class="table align-items-center mt-5" style="font-size:12px;" id="myTable">
              <thead>
                <tr>
                  <th>Merchant Name</th>
                  <th>Seller Name</th>
                  <th>Address</th>
                  <th>Phone</th>
                  <th>Email</th>
                  <th>Type</th>
                  <th>Register Date</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $k => $item)
                <tr align="center">

                  <td>{{$item->me_name}}</td>
                  <td>{{$item->me_seller_name}}</td>
                  <td>
                    {{$item->me_address}}
                  </td>
                  <td>{{$item->me_phone}}</td>
                  <td>
                    {{$item->me_email}}
                  </td>
                  <td>{{$item->me_type}}</td>
                  <td>{{ \Carbon\Carbon::parse($item->me_register_date)->format('Y-m-d') }}</td>
                  <td class="align-middle">
                    <a href="{{ route('merchant_form_edit', ['id' => $item->me_id,'custom' => 2]) }}" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                      data-original-title="Edit user">
                      Edit
                    </a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('merchant_delete', ['id' => $item->me_id,'userId' => $item->me_ld_id]) }}" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                      data-original-title="Edit user">
                      Delete
                    </a>
                  </td>
                </tr>
                @endforeach


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
