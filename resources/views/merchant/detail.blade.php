@extends('welcome')

@section('content')
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <div id="generic_price_table">
            <section>
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <!--PRICE HEADING START-->
                    <div class="price-heading clearfix">
                      <h1>Merchant</h1>
                    </div>
                    <!--//PRICE HEADING END-->
                  </div>
                </div>
              </div>
              <div class="container">

                <!--BLOCK ROW START-->
                <div class="row">

                  <div class="col-md-12">

                    <!--PRICE CONTENT START-->
                    <div class="generic_content active clearfix">

                      <!--HEAD PRICE DETAIL START-->
                      <div class="generic_head_price clearfix">

                        <!--HEAD CONTENT START-->
                        <div class="generic_head_content clearfix">

                          <!--HEAD START-->
                          <div class="head_bg"></div>
                          <div class="head">
                            <span>{{$dt->me_seller_name}}</span>
                          </div>
                          <!--//HEAD END-->

                        </div>
                        <!--//HEAD CONTENT END-->

                        <!--PRICE START-->
                        <div class="generic_price_tag clearfix">
                          <span class="price">
                            <span class="currency">{{$dt->me_name}}</span>

                          </span>
                        </div>
                        <!--//PRICE END-->

                      </div>
                      <!--//HEAD PRICE DETAIL END-->

                      <!--FEATURE LIST START-->
                      <div class="generic_feature_list" style="background-color: #f6f6f6;">
                        <table width="100%">
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>Username</td>
                            <td>=</td>
                            <td>{{$dt->ld_username}}</td>
                          </tr>
                          <tr>
                            <td>Address</td>
                            <td>=</td>
                            <td>{{$dt->me_address}}</td>
                          </tr>
                          <tr>
                            <td>Phone</td>
                            <td>=</td>
                            <td style="font-family:sans-serif;">{{$dt->	me_phone}}</td>
                          </tr>
                          <tr>
                            <td>Email</td>
                            <td>=</td>
                            <td>{{$dt->me_email}}</td>
                          </tr>
                          <tr>
                            <td>Type</td>
                            <td>=</td>
                            <td>{{$dt->me_type}}</td>
                          </tr>
                          <tr>
                            <td>Register Date</td>
                            <td>=</td>
                            <td>{{ \Carbon\Carbon::parse($dt->me_register_date)->format('d-m-Y') }}</td>
                          </tr>
                          <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                          </tr>
                        </table>
                      </div>
                      <!--//FEATURE LIST END-->

                      <!--BUTTON START-->
                      <div class="generic_price_btn clearfix" style="margin-top:50px;">
                      <a href="{{ route('merchant_form_edit', ['id' => $dt->me_id,'custom' => 1]) }}">
                      Edit
                    </a>
                      </div>
                      
                      <!--//BUTTON END-->

                    </div>
                    <!--//PRICE CONTENT END-->

                  </div>
                </div>
                <!--//BLOCK ROW END-->

              </div>
            </section>

          </div>
          <!-- partial -->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
