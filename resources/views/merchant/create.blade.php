@extends('welcome')

@section('content')
<div class="container-fluid py-4">
    <form action="{{ route('merchant_insert') }}" method="post">
        @csrf
        <input type="hidden" value="" name="id">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <h4>Merchant</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Merchant Name</label>
                                    <input name="MerchantName" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Seller Name</label>
                                    <input name="SellerName" class="form-control" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Phone</label>
                                    <input name="phone" class="form-control" type="number">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Email</label>
                                    <input name="email" class="form-control" type="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Address</label>
                                    <textarea name="address" class="form-control" cols="30" rows="6"></textarea>
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Merchant Type</label>
                                    <input class="form-control" type="text" name="merchantType">
                                </div>
                            </div>
                        </div>


                        <input type="submit" value="Save" class="btn btn-primary">
                        <a href="{{ route('merchant') }}" class="btn btn-primary" data-toggle="tooltip">
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
