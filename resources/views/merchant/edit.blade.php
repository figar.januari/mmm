@extends('welcome')

@section('content')
<div class="container-fluid py-4">
  <form action="{{ route('merchant_update') }}" method="post">
    @csrf
    <input type="hidden" value="{{$dt->me_id}}" name="id">
    <input type="hidden" value="{{$custom}}" name="custom">
    <div class="row">
      <div class="col-md-12">
        <div class="card">

          <div class="card-body">
            <h4>Merchant</h4>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="example-text-input" class="form-control-label">Merchant Name</label>

                  <input name="MerchantName" class="form-control" type="text" value="{{$dt->me_name}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="example-text-input" class="form-control-label">Seller Name</label>
                  <input name="SellerName" class="form-control" type="text" value="{{$dt->me_seller_name}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="example-text-input" class="form-control-label">Merchant Type</label>
                  <input class="form-control" type="text" name="merchantType" value="{{$dt->me_type}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="example-text-input" class="form-control-label">Phone</label>
                  <input name="phone" class="form-control" type="number" value="{{$dt->me_phone}}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">

                  <label for="example-text-input" class="form-control-label">Address</label>
                  <textarea name="address" class="form-control" cols="30" rows="6"
                    value="{{$dt->me_address}}">{{$dt->me_address}}</textarea>

                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="example-text-input" class="form-control-label">Username</label>
                  <input name="Usename" class="form-control" type="text" value="{{$dt->ld_username}}" @if ($custom==2)
                    readonly="readonly" @endif>
                  <label for="example-text-input" class="form-control-label">Email</label>
                  <input name="email" class="form-control" type="email" value="{{$dt->me_email}}">


                </div>
              </div>
            </div>


            <input type="submit" value="Save" class="btn btn-primary">
            <a href="{{ route('merchant') }}" class="btn btn-primary" data-toggle="tooltip">
              Back
            </a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection
