@extends('welcome')

@section('content')
<div class="container-fluid py-4">
    <form action="{{ route('product_update') }}" method="post">
        @csrf
        <input type="hidden" value="" name="id">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <h4>Product</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <!-- Masih di hardcode -->
                                    <input name="MerchantName" class="form-control" type="hidden"
                                        value="{{$dt->merchants_id}}">
                                    <input name="ProductId" class="form-control" type="hidden"
                                        value="{{$dt->product_id}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Product Name</label>
                                    <input name="ProductName" class="form-control" type="text"
                                        value="{{$dt->product_name}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Product Code</label>
                                    <input name="ProductCode" class="form-control" type="text"
                                        value="{{$dt->product_code}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Product Type</label>
                                    <select class="form-control" name="ProductType" id="category">
                                        <option hidden>Choose Category</option>
                                        @foreach ($data as $item)
                                        <option value="{{ $item->product_type_id }}" @if ($item->product_type_id ==
                                            $dt->product_type_id)
                                            selected="selected"
                                            @endif
                                            >
                                            {{ $item->product_type_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Stock</label>
                                    <input class="form-control" type="Number" name="stock" value="{{$dt->stock}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Price</label>
                                    <input class="form-control" type="Number" name="price" value="{{$dt->price}}">
                                </div>
                            </div>
                        </div>


                        <input type="submit" value="Edit" class="btn btn-primary">
                        <a href="{{ route('product') }}" class="btn btn-primary" data-toggle="tooltip">
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
