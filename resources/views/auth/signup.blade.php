<!--
=========================================================
* Argon Dashboard 2 - v2.0.4
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('storage/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('storage/img/favicon.png') }}">
    <title>
        Merchant Money Management
    </title>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <!-- Nucleo Icons -->
    <link href="{{ asset('storage/css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('storage/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <link href="{{ asset('storage/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('storage/css/argon-dashboard.css?v=2.0.4') }}" rel="stylesheet" />
    </head>

<body class="">
  <main class="main-content  mt-0 mb-5">
    <div class="page-header align-items-start min-vh-50 pt-5 pb-11 m-3 border-radius-lg" style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/signup-cover.jpg'); background-position: top;">
      <span class="mask bg-gradient-dark opacity-6"></span>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-5 text-center mx-auto">
            <h1 class="text-white mb-2 mt-5">Selamat Datang!</h1>
            <p class="text-lead text-white">Hanya dengan isi beberapa data dibawah kamu sudah bisa menggukan <strong>MMM</strong></p>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row mt-lg-n10 mt-md-n11 mt-n10 justify-content-center">
        <div class="col-xl-4 col-lg-5 col-md-7 mx-auto">
          <div class="card z-index-0">
            <div class="card-body">
              <form role="form" action="{{ route('do_register') }}" method="post">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                @csrf
                <div class="mb-3">
                  <input type="text" class="form-control" placeholder="Nama Pengguna" aria-label="usernmae" name="username">
                </div>
                <div class="mb-3">
                  <input type="text" class="form-control" placeholder="Nama Lengkap" aria-label="fullname" name="fullname">
                </div>
                <div class="mb-3">
                  <input type="text" class="form-control" placeholder="Nama Toko" aria-label="storeName" name="storeName">
                </div>
                <div class="mb-3">
                  <input type="email" name="email" class="form-control" placeholder="Email" aria-label="email">
                </div>
                <div class="mb-3">
                  <textarea class="form-control" name="address" placeholder="Alamat Lengkap" id="" cols="30" rows="6"></textarea>
                </div>
                <div class="mb-3">
                  <input type="number" name="phoneNumber" class="form-control" placeholder="Nomor Hp" aria-label="phone-number">
                </div>
                <div class="mb-3">
                  <input type="text" name="type" class="form-control" placeholder="Jenis Usaha" aria-label="type">
                </div>
                <div class="mb-3">
                  <input type="password" name="password" class="form-control" placeholder="Kata Sandi" aria-label="password">
                </div>
                <div class="text-center">
                  <button type="submit" class="btn bg-gradient-dark w-100 my-4 mb-2">Daftar</button>
                </div>
                <p class="text-sm mt-3 mb-0">Sudah punya akun? <a href="javascript:;" class="text-dark font-weight-bolder">Masuk</a></p>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <!--   Core JS Files   -->
  <script src="{{ asset('storage/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('storage/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('storage/js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('storage/js/plugins/smooth-scrollbar.min.js') }}"></script>
  <script src="{{ asset('storage/js/plugins/chartjs.min.js') }}"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('storage/js/argon-dashboard.min.js?v=2.0.4') }}"></script>
</body>

</html>
