@extends('welcome')

@section('content')

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <H3>Product Type</H3>

                </div>

                <div class="card-body p-5">
                    <a href="{{ route('product_type_form_create') }}" class="btn btn-primary" data-toggle="tooltip"
                        data-original-title="Edit user">
                        Create
                    </a>
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mt-5" style="font-size:12px;" id="myTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Product Type</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $k => $item)
                                <tr>
                                    <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{++$k}}</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$item->product_type_name}}</td>
                                    <td class="align-middle">
                                        <a href="{{ route('product_type_form_edit', ['id' => $item->product_type_id]) }}" class="text-secondary font-weight-bold text-xs"
                                            data-toggle="tooltip" data-original-title="Edit user">
                                            Edit
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="{{ route('product_type_delete', ['id' => $item->product_type_id]) }}" class="text-secondary font-weight-bold text-xs"
                                            data-toggle="tooltip" data-original-title="Edit user">
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
