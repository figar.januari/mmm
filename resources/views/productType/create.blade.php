@extends('welcome')

@section('content')
<div class="container-fluid py-4">
    <form action="{{ route('product_type_insert') }}" method="post">
        @csrf
        <input type="hidden" value="" name="id">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <h4>Product Type</h4>
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="example-text-input" class="form-control-label">Product Type Name</label>
                                    <input name="ProductTypeName" class="form-control" type="text">
                                </div>
                            </div>

                        </div>


                        <input type="submit" value="Save" class="btn btn-primary">
                        <a href="{{ route('product_type') }}" class="btn btn-primary" data-toggle="tooltip">
                            Back
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
