@extends('welcome')

@section('content')
<div class="container-fluid py-4">
  <form action="{{ route('transactions_detail_insert') }}" method="post">
    @csrf
    <input type="hidden" value="" name="id">
    <div class="row">
      <div class="col-md-12">
        <div class="card">

          <div class="card-body">
            <h4>Create Transaction</h4>
            <div class="row">

              <div class="col-md-8">
                <div class="form-group">
                  <label for="example-text-input" class="form-control-label">Product Type</label>
                  <select class="js-example-basic-single form-control" name="product" id="product">
                    <option value=""> Select Option</option>
                    @foreach ($data as $item)
                    <option value="{{ $item->product_id }}">{{ $item->product_name }}
                    </option>
                    @endforeach
                  </select>
                </div>
                <div>
                  <label for="example-text-input" class="form-control-label">Stock</label>
                  <input class="form-control" type="Number" id="stock" name="stock" value="">
                  <input class="form-control" type="hidden" id="me_id" name="me_id" value="{{$data[0] -> me_id}}">
                  <input class="form-control" type="hidden" id="sum_tr" name="sum_tr" value="{{$sum_tr}}">
                  <input class="form-control" type="hidden" id="total_stock" name="total_stock" value="">
                  <input class="form-control" type="hidden" id="tr_id" name="tr_id" value="{{$tr_id}}">
                </div>
                <div class="mb-3">
                  <label for="example-text-input" class="form-control-label">Total Price</label>
                  <input class="form-control" type="Number" id="price" name="price" value="" readonly>
                  <input class="form-control" type="hidden" id="unit_price" name="unit_price" value="" readonly>
                </div>
              </div>
            </div>


            <input type="submit" value="Save" class="btn btn-primary">
            <a href="{{ route('transactions') }}" class="btn btn-primary" data-toggle="tooltip">
              Back
            </a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script>
$('#stock').on('change', function () {
const stock = parseInt(this.value);
const tot_stock = parseInt($('#total_stock').val());

if(stock <= tot_stock){
    const unit_price = $('#unit_price').val();
    const total_price = stock * unit_price;
    $('#price').val(total_price.toFixed(2));
}
else{
    alert("Sorry, the item is out of stock. Stock available only " + tot_stock);
    $('#stock').val(tot_stock);
}
});

  $('#product').on('change', function () {
    //alert(this.value);
    const id = this.value;
    //alert(window.location.origin);
    let HostName = window.location.origin;
    var url = HostName + "/transaction/GetDataProduct/" + id;
    //alert(url);
    $.getJSON(url, function (data) {
      alert(data["stock"]);

      $('#unit_price').val(data["price"]);
      $('#total_stock').val(data["stock"]);
    });
  });

</script>

@endsection
