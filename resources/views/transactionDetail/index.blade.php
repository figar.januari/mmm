@extends('welcome')

@section('content')

<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          <H3>Transactions Details</H3>

        </div>

        <div class="card-body p-5">
          <a href="{{ route('add_transaction', ['me_id' => $me_id,'tr_id' => $data[0]->tr_id ] )}}" class="btn btn-primary" data-toggle="tooltip"
            data-original-title="Edit user">
            Create Transaction
          </a>
          <div class="table-responsive p-0">
            <table class="table align-items-center mt-5 nowrap" style="font-size:12px;" id="myTable">
              <thead>
                <tr>
                  <th>Transaction Type</th>
                  <th>Product Name</th>
                  <th>Stock</th>
                  <th>Unit price</th>
                  <th>Total Price</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $k => $item)
                <tr align="center">

                  <td>{{$type}}</td>
                  <td>{{$item->product_name}}</td>
                  <td>{{$item->total_stock}}</td>
                  <td>{{$item->price}}</td>
                  <td>
                    {{$item->total_price}}
                  </td>
                  
                  <td class="align-middle">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="{{ route('transactions_dt_delete', ['id' => $item->tr_dt_id, 'tr_id' => $item->tr_id]) }}"
                      class="text-secondary font-weight-bold text-xs" data-toggle="tooltip"
                      data-original-title="Edit user">
                      Delete
                    </a>
                  </td>
                </tr>
                @endforeach


              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
