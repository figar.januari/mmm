<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductTypeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\MerchantController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\TransactionDetail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/signup', [AuthController::class, 'signup'])->name('signup');
Route::post('/do_register', [AuthController::class, 'doRegister'])->name('do_register');

Route::get('/', function () {
  return view('auth.login');
})->name('login');
Route::post('/do_login', [AuthController::class, 'do_login'])->name('do_login');
Route::get('/do_logout', [AuthController::class, 'do_logout'])->name('do_logout');

Route::get('/dashboard', [AuthController::class, 'dashboard'])->name('dashboard');

Route::prefix('transaction')->group(function() {
    Route::get('/', function () {
        return view('profile.keren');
    })->name('trx');

    Route::get('/add', function () {
        return view('trx.trx_add');
    })->name('add_trx');
});

Route::prefix('profile')->group(function() {
    Route::get('/', [ProfileController::class, 'index'])->name('profile');
    Route::post('/update', [ProfileController::class, 'update'])->name('profile_update');
    Route::post('/insert', [ProfileController::class, 'insert'])->name('profile_insert');
    Route::get('/delete/{id}', [ProfileController::class, 'delete'])->name('profile_delete');
});

Route::prefix('products')->group(function() {
    Route::get('/', [ProductController::class, 'index'])->name('product');
    Route::get('/create', [ProductController::class, 'create'])->name('product_form_create');
    Route::post('/insert', [ProductController::class, 'insert'])->name('product_insert');
    Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product_form_edit');
    Route::post('/update', [ProductController::class, 'update'])->name('product_update');
    Route::get('/delete/{id}', [ProductController::class, 'delete'])->name('product_delete');
});

Route::prefix('productType')->group(function() {
    Route::get('/', [ProductTypeController::class, 'index'])->name('product_type');
    Route::get('/create', [ProductTypeController::class, 'create'])->name('product_type_form_create');
    Route::post('/insert', [ProductTypeController::class, 'insert'])->name('product_type_insert');
    Route::get('/edit/{id}', [ProductTypeController::class, 'edit'])->name('product_type_form_edit');
    Route::post('/update', [ProductTypeController::class, 'update'])->name('product_type_update');
    Route::get('/delete/{id}', [ProductTypeController::class, 'delete'])->name('product_type_delete');
});

Route::prefix('merchants')->group(function() {
    Route::get('/', [MerchantController::class, 'index'])->name('merchant');
    Route::get('/detail', [MerchantController::class, 'detail'])->name('merchant_detail');
    Route::get('/create', [MerchantController::class, 'create'])->name('merchant_form_create');
    Route::post('/insert', [MerchantController::class, 'insert'])->name('merchant_insert');
    Route::get('/edit/{id}+{custom}', [MerchantController::class, 'edit'])->name('merchant_form_edit');
    Route::post('/update', [MerchantController::class, 'update'])->name('merchant_update');
    Route::get('/delete/{id}+{userId}', [MerchantController::class, 'delete'])->name('merchant_delete');
});

Route::prefix('transaction')->group(function() {

    Route::get('/GetDataProduct/{id}', [TransactionController::class, 'GetDataProduct'])->name('dataProduct');
    Route::get('/index', [TransactionController::class, 'index'])->name('transactions');
    Route::get('/create/{id}', [TransactionController::class, 'create'])->name('transactions_form_create');
    Route::post('/insert', [TransactionController::class, 'insert'])->name('transactions_insert');
    Route::get('/edit/{id}', [TransactionController::class, 'edit'])->name('transactions_form_edit');
    Route::post('/update', [TransactionController::class, 'update'])->name('transactions_update');
    Route::get('/delete/{id}', [TransactionController::class, 'delete'])->name('transactions_delete');
});

Route::prefix('transaction_detail')->group(function() {
    Route::get('/index/{id}+{type}+{me_id}', [TransactionDetail::class, 'index'])->name('transactions_detail');
    Route::get('/AddTransaction/{me_id}+{tr_id}', [TransactionDetail::class, 'add_transaction'])->name('add_transaction');
    Route::post('/insert', [TransactionDetail::class, 'insert'])->name('transactions_detail_insert');
    Route::get('/delete/{id}+{tr_id}', [TransactionDetail::class, 'delete'])->name('transactions_dt_delete');
});
